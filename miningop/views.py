import logging

from django_celery_beat.models import PeriodicTask
from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required
from django.db.models import Q
from django.shortcuts import redirect, render
from django.utils import timezone
from esi.decorators import token_required

from allianceauth.eveonline.models import EveCharacter
from allianceauth.views import NightModeRedirectView

from .models import MiningOp, MiningOpMember
from .tasks import (
    update_ledgers,
    update_ledger,
    update_members,
    update_ledgers_manually,
)

logger = logging.getLogger(__name__)


def get_characters(request):
    return (
        EveCharacter.objects.filter(character_ownership__user=request.user)
        .select_related()
        .order_by("character_name")
    )


@login_required
@permission_required("Mining Ops.basic_access")
def index(request):
    characters = get_characters(request)
    character_ids = [c.character_id for c in characters]

    belongs_to_members = MiningOpMember.objects.filter(
        character__character_id__in=character_ids
    )

    if request.user.has_perm("Mining Ops.manage"):
        inactive_mining_ops = MiningOp.objects.filter(
            completed_at__isnull=False
        ).order_by("-created_at")
    else:
        belongs_to_ids = [m.mining_op_id for m in belongs_to_members]

        inactive_mining_ops = (
            MiningOp.objects.filter(
                Q(id__in=belongs_to_ids)
                | Q(fleet_commander__character_id__in=character_ids)
            )
            .filter(completed_at__isnull=False)
            .order_by("-created_at")
        )

    active_mining_ops = MiningOp.objects.filter(completed_at__isnull=True).order_by(
        "-created_at"
    )

    context = {
        "can_manage": request.user.has_perm("Mining Ops.manage"),
        "missing_tokens": ", ".join(
            set(
                [
                    m.character.character_name
                    for m in belongs_to_members
                    if not m.ledger_token
                ]
            )
        ),
        "active_mining_ops": active_mining_ops,
        "inactive_mining_ops": inactive_mining_ops,
    }

    return render(request, "miningop/index.html", context)


@login_required
@permission_required("Mining Ops.basic_access")
def view(request, mining_op_id):
    update_task = PeriodicTask.objects.get(
        name="miningop_tasks_update_ledgers_periodically"
    )

    update_timedelta = (
        int(update_task.schedule.is_due(update_task.last_run_at)[1])
        if update_task
        else None
    )

    # TODO: this should not be hardcoded
    update_interval = 600

    mining_op = MiningOp.objects.get(pk=mining_op_id)

    characters = get_characters(request)
    character_ids = [c.character_id for c in characters]

    members = mining_op.members.filter(character__character_id__in=character_ids)

    context = {
        "can_manage": request.user.has_perm("Mining Ops.manage"),
        "missing_tokens": ", ".join(
            set([m.character.character_name for m in members if not m.ledger_token])
        ),
        "is_night_mode": NightModeRedirectView.night_mode_state(request),
        "mining_op": mining_op,
        "update_timedelta": update_timedelta,
        "update_interval": update_interval,
    }

    return render(request, "miningop/view.html", context)


@login_required
@token_required(
    scopes=[
        "esi-fleets.read_fleet.v1",
        "esi-fleets.write_fleet.v1",
        "esi-location.read_location.v1",
    ]
)
@permission_required("Mining Ops.manage")
def create(request, token):
    character = EveCharacter.objects.get_character_by_id(token.character_id)
    if not character:
        context = {
            "character_id": token.character_id,
            "character_name": token.character_name,
            "character_portrait_url": EveCharacter.generic_portrait_url(
                token.character_id, 128
            ),
        }

        return render(request, "miningop/characternotexisting.html", context=context)

    mining_op = MiningOp(fleet_commander=character)
    mining_op.save()

    mining_op.members.create(character=character)

    messages.success(request, "Mining op created")

    return redirect("miningop:index")


@login_required
@permission_required("Mining Ops.manage")
def start(request, mining_op_id):
    mining_op = MiningOp.objects.get(pk=mining_op_id)

    update_members(mining_op)
    update_ledgers(mining_op)

    mining_op.started_at = timezone.now()
    mining_op.save()

    messages.success(request, "Mining op started")

    return redirect("miningop:view", mining_op_id=mining_op_id)


@login_required
@permission_required("Mining Ops.manage")
def refresh(request, mining_op_id):
    mining_op = MiningOp.objects.get(pk=mining_op_id)

    update_members(mining_op)
    update_ledgers(mining_op)

    messages.success(request, "Mining op refreshed")

    return redirect("miningop:view", mining_op_id=mining_op_id)


@login_required
@permission_required("Mining Ops.manage")
def complete(request, mining_op_id):
    mining_op = MiningOp.objects.get(pk=mining_op_id)

    update_members(mining_op)
    update_ledgers(mining_op)

    mining_op.completed_at = timezone.now()
    mining_op.save()

    update_ledgers_manually.apply_async(countdown=600, args=(mining_op.id,))

    messages.success(request, "Mining op completed")

    return redirect("miningop:view", mining_op_id=mining_op_id)


@login_required
@token_required(scopes=["esi-industry.read_character_mining.v1"])
@permission_required("Mining Ops.basic_access")
def authenticate_index(request, token):
    return redirect("miningop:index")


@login_required
@token_required(scopes=["esi-industry.read_character_mining.v1"])
@permission_required("Mining Ops.basic_access")
def authenticate_view(request, token, mining_op_id):
    mining_op = MiningOp.objects.get(pk=mining_op_id)
    member = mining_op.members.filter(
        character__character_id=token.character_id
    ).first()

    if member:
        update_ledger(mining_op, member)

    return redirect("miningop:view", mining_op_id=mining_op_id)
