import datetime

from django.core.cache import cache
from django.db import models
from django.utils import timezone
from django.utils.functional import cached_property
from esi.models import Token

from allianceauth.eveonline.models import EveCharacter
from allianceauth.eveonline.providers import provider

from .helpers import get_prices


class LedgerDiff:
    def __init__(self):
        self.minerals = {}
        self.quantities = {}
        self.volumes = {}

    @cached_property
    def details(self):
        result = []

        prices = get_prices(self.quantities.keys())

        for type_id, quantity in self.quantities.items():
            result.append(
                {
                    "type_id": type_id,
                    "name": self.minerals[type_id]["name"],
                    "quantity": quantity,
                    "volume": self.volumes[type_id],
                    "price_per_unit": prices[type_id]["buy"],
                    "price_after_tax": prices[type_id]["buy"] * quantity * 0.95,
                    "price_total": prices[type_id]["buy"] * quantity,
                }
            )

        return result

    @property
    def total_payout(self):
        return sum(x["price_after_tax"] for x in self.details)

    @property
    def total_units(self):
        return sum(self.quantities.values())


class MiningOp(models.Model):
    fleet_commander = models.ForeignKey(
        EveCharacter, null=True, on_delete=models.SET_NULL
    )
    created_at = models.DateTimeField(default=timezone.now)
    started_at = models.DateTimeField(null=True)
    completed_at = models.DateTimeField(null=True)

    @property
    def fleet_token(self):
        valid_tokens = (
            Token.objects.filter(character_id=self.fleet_commander.character_id)
            .require_scopes(["esi-fleets.read_fleet.v1"])
            .require_valid()
        )

        return valid_tokens[0] if valid_tokens else None

    @property
    def status(self):
        if not self.fleet_token:
            return "No fleet token"

        if self.completed_at:
            return "Completed"

        if self.started_at:
            return "In progress"

        return "Waiting to start"
    
    @property
    def total_units(self):
        return sum(m.ledger_diff.total_units for m in self.members.all())
    
    @property
    def total_payout(self):
        return sum(m.ledger_diff.total_payout for m in self.members.all())

    class Meta:
        default_permissions = ()
        permissions = (
            ("basic_access", "Can access Mining Op module"),
            ("manage", "Can manage mining ops"),
        )


class MiningOpMember(models.Model):
    mining_op = models.ForeignKey(
        MiningOp, related_name="members", on_delete=models.CASCADE
    )
    character = models.ForeignKey(EveCharacter, null=True, on_delete=models.SET_NULL)
    initial_ledger = models.JSONField(null=True)
    current_ledger = models.JSONField(null=True)
    initial_ledger_fetched_at = models.DateTimeField(null=True)
    created_at = models.DateTimeField(default=timezone.now)

    @cached_property
    def ledger_token(self):
        valid_tokens = (
            Token.objects.filter(character_id=self.character.character_id)
            .require_scopes(["esi-industry.read_character_mining.v1"])
            .require_valid()
        )

        return valid_tokens[0] if valid_tokens else None

    @cached_property
    def ledger_diff(self):
        result = LedgerDiff()
        if self.initial_ledger is None or self.current_ledger is None:
            return result

        # This could possibly be improved
        initial_upt = self._get_ledger_units(self.initial_ledger)
        current_upt = self._get_ledger_units(self.current_ledger)

        diff_upt = {
            x: current_upt[x] - (initial_upt[x] if x in initial_upt else 0)
            for x in current_upt
        }

        for type_id in diff_upt.keys():
            quantity = diff_upt[type_id]
            if not quantity:
                continue

            cache_key = "mineral:{}".format(type_id)

            mineral = cache.get(cache_key)
            if not mineral:
                mineral = provider.client.Universe.get_universe_types_type_id(
                    type_id=type_id
                ).result()
                cache.set(cache_key, mineral)

            result.minerals[type_id] = mineral
            result.quantities[type_id] = quantity
            result.volumes[type_id] = diff_upt[type_id] * mineral["volume"]

        return result

    def _get_ledger_units(self, ledger):
        start_date = self.created_at.date() - datetime.timedelta(days=1)

        units_per_type = {}
        for entry in ledger:
            date = datetime.datetime.strptime(entry["date"], "%Y-%m-%d").date()
            if date < start_date:
                continue

            type_id = entry["type_id"]
            quantity = entry["quantity"]

            if type_id not in units_per_type:
                units_per_type[type_id] = 0

            units_per_type[type_id] += quantity

        return units_per_type

    class Meta:
        default_permissions = ()
        permissions = ()
