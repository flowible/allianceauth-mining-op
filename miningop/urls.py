from django.urls import path

from . import views

app_name = "miningop"

urlpatterns = [
    path("", views.index, name="index"),
    path("ops/<int:mining_op_id>", views.view, name="view"),
    path("ops/<int:mining_op_id>/start", views.start, name="start"),
    path("ops/<int:mining_op_id>/complete", views.complete, name="complete"),
    path("ops/<int:mining_op_id>/refresh", views.refresh, name="refresh"),
    path(
        "ops/<int:mining_op_id>/authenticate", views.authenticate_view, name="authenticate_view"
    ),
    path("create", views.create, name="create"),
    path("authenticate", views.authenticate_index, name="authenticate_index"),
]
