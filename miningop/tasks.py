from concurrent.futures import ThreadPoolExecutor

from bravado.exception import HTTPError, HTTPNotFound
from django.utils import timezone
from celery import shared_task, group

from allianceauth.eveonline.models import EveCharacter
from allianceauth.eveonline.providers import SWAGGER_SPEC_PATH
from allianceauth.services.hooks import get_extension_logger

from .models import MiningOp, MiningOpMember

logger = get_extension_logger(__name__)


def update_members(mining_op):
    token = mining_op.fleet_token
    client = token.get_esi_client(spec_file=SWAGGER_SPEC_PATH)

    try:
        fleet = client.Fleets.get_characters_character_id_fleet(
            character_id=token.character_id
        ).result()
    except HTTPNotFound:
        logger.debug("FC is not in a fleet for mining op ID {}".format(mining_op.id))
        return
    except HTTPError:
        logger.exception(
            "Failed to fetch fleet information for mining op ID {}".format(mining_op.id)
        )
        return

    fleet_id = fleet["fleet_id"]

    logger.debug(
        "Fetching fleet members for mining op ID {}, fleet ID {}".format(
            mining_op.id, fleet_id
        )
    )

    try:
        fleet_members = client.Fleets.get_fleets_fleet_id_members(
            fleet_id=fleet_id
        ).result()
    except HTTPError:
        logger.exception(
            "Failed to fetch fleet members for mining op ID {}, fleet ID {}".format(
                mining_op.id, fleet_id
            )
        )
        return

    for fleet_member in fleet_members:
        character_id = fleet_member["character_id"]

        if mining_op.members.filter(character__character_id=character_id).exists():
            continue

        character = EveCharacter.objects.filter(character_id=character_id).first()
        if not character:
            character = EveCharacter.objects.create_character(character_id)

        member = mining_op.members.create(character=character)
        update_ledger(mining_op, member)


def update_ledgers(mining_op):
    with ThreadPoolExecutor(max_workers=10) as executor:
        for member in mining_op.members.all():
            executor.submit(update_ledger, mining_op, member)


def update_ledger(mining_op, member):
    token = member.ledger_token

    if not token:
        return

    client = token.get_esi_client(spec_file=SWAGGER_SPEC_PATH)

    try:
        ledger = client.Industry.get_characters_character_id_mining(
            character_id=token.character_id
        ).result()
    except HTTPError:
        logger.exception(
            "Failed to fetch mining ledger for mining op ID {}, character ID {}".format(
                mining_op.id, token.character_id
            )
        )
        return

    fix_ledger(ledger)

    if member.initial_ledger is None:
        member.initial_ledger = ledger
        member.initial_ledger_fetched_at = timezone.now()

    member.current_ledger = ledger
    member.save()


def get_active_ops():
    return MiningOp.objects.filter(started_at__isnull=False, completed_at__isnull=True)


def fix_ledger(ledger):
    for entry in ledger:
        entry["date"] = str(entry["date"])


@shared_task
def update_members_periodically():
    for mining_op in get_active_ops():
        update_members(mining_op)


@shared_task
def update_ledgers_periodically():
    for mining_op in get_active_ops():
        g = group(
            update_single_ledger_periodically.s(mining_op.id, member.id)
            for member in mining_op.members.all()
        )

        g.apply_async()


@shared_task
def update_single_ledger_periodically(mining_op_id, member_id):
    mining_op = MiningOp.objects.get(pk=mining_op_id)
    member = MiningOpMember.objects.get(pk=member_id)

    update_ledger(mining_op, member)


@shared_task
def update_ledgers_manually(mining_op_id):
    mining_op = MiningOp.objects.get(pk=mining_op_id)
    update_ledgers(mining_op)
