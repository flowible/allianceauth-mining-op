import requests

from django.core.cache import cache


def get_prices(type_ids):
    if len(type_ids) == 0:
        return {}

    cache_keys = list(map(_get_cache_key, type_ids))
    cache_entries = cache.get_many(cache_keys)

    if all(key in cache_entries for key in cache_keys):
        return {type_id: cache_entries[_get_cache_key(type_id)] for type_id in type_ids}

    response = requests.get(
        "https://api.evemarketer.com/ec/marketstat/json",
        params={
            "typeid": ",".join([str(x) for x in type_ids]),
            "regionlimit": 10000002,
        },
    )

    items = response.json()

    result = {}
    for item in items:
        item_id = item["buy"]["forQuery"]["types"][0]

        result[item_id] = {
            "buy": item["buy"]["max"],
            "sell": item["sell"]["min"],
        }

    cache.set_many(
        {_get_cache_key(type_id): price for type_id, price in result.items()}, 300
    )

    return result


def _get_cache_key(type_id):
    return "prices:{}".format(type_id)
