from django.apps import AppConfig


class MiningOpConfig(AppConfig):
    name = "miningop"
    label = "Mining Ops"
    verbose_name = "Mining Operations"
