These need to be added to local.py

> CELERYBEAT_SCHEDULE['miningop_tasks_update_members_periodically'] = {
>     'task': 'miningop.tasks.update_members_periodically',
>     'schedule': crontab(minute='*/1'),
> }
> 
> CELERYBEAT_SCHEDULE['miningop_tasks_update_ledgers_periodically'] = {
>     'task': 'miningop.tasks.update_ledgers_periodically',
>     'schedule': crontab(minute='*/10'),
> }
